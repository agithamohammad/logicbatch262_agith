﻿using System;

namespace ProjectLogicDay1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Silahkan pilih soal 1/2/3/4/5 : ");
            string pilih = Console.ReadLine();

            switch (pilih)
            {
                case "1":
                    //soal1();
                    soal1 s1 = new soal1();
                    s1.soalnilai();
                    break;
                case "2":
                    soal2();
                    break;
                case "3":
                    soal3();
                    break;
                case "4":
                    soal4();
                    break;
                case "5":
                    soal5();
                    break;
                default:
                    break;
            }
            Console.ReadKey();
        }

        static void soal1()
        {
            int nilai;
            Console.Write("Masukan nilai : ");
            nilai = int.Parse(Console.ReadLine());

            if ( nilai >= 85)
            {
                Console.WriteLine("A");
            }
            else if ( nilai >= 70 )
            {
                Console.WriteLine("B");
            }
            else if ( nilai >= 55)
            {
                Console.WriteLine("C");
            }
            
        }

        static void soal2()
        {
            int poin;
            Console.WriteLine(" Pulsa : ");
            int pulsa = int.Parse(Console.ReadLine());

            if (pulsa < 25000)
            {
                poin = 80;
            }else if (pulsa < 50000)
            {
                poin = 200;
            }else if (pulsa < 100000)
            {
                poin = 400;
            }
            else
            {
                poin = 800;
            }
            Console.WriteLine("Pulsa :   "+pulsa);
            Console.WriteLine("Poin :   "+poin);

        }

        static void soal3()
        {
            int totalbel, belanja, jarak, km, ongkir;
            float diskon;

            km = 1000;
            diskon = 0;

            Console.WriteLine("Belanja : ");
            belanja = int.Parse(Console.ReadLine());
            Console.WriteLine("Jarak : ");
            jarak = int.Parse(Console.ReadLine());
            Console.WriteLine("Masukan Promo : ");
            string promo = Console.ReadLine();

            Console.WriteLine("Belanja :                 " + belanja);
            bool isKodeValid = promo == "JKTOVO";


            if (belanja >= 30000 && isKodeValid)
            {
                diskon = (float)(belanja * 0.4);
                Console.WriteLine("Diskon 40% :           " + diskon);
            }
            else
            {
                Console.WriteLine("Diskon 40% :           " + diskon);
            }

            if (jarak > 5)
            {
                ongkir = jarak * km;
                Console.WriteLine("Ongkir:     " + ongkir);
            }
            else
            {
                ongkir = 5000;
                Console.WriteLine("Ongkir:     " + ongkir);
            }

            totalbel = (int)(belanja + diskon + ongkir); 
            Console.WriteLine("Total Belanja :              "+totalbel);
        }

        static void soal4()
        {
            int Tongkir,belanja,ongkir,Tbelanja,total;

            Tongkir = 0;
            Tbelanja = 0;

            Console.WriteLine("Belanja : ");
            belanja = int.Parse(Console.ReadLine());
            Console.WriteLine("Ongkos Kirim : ");
            ongkir = int.Parse(Console.ReadLine());
            Console.WriteLine("Pilih Voucher : ");
            string voucher = Console.ReadLine();

            switch (voucher)
            {
                case "1":
                    if (belanja >= 30000)
                    {
                        Tongkir = ongkir - 5000;
                        Tbelanja = belanja - 5000;
                    }
                break;

                case "2":
                    if (belanja >= 50000)
                    {
                        Tongkir = ongkir - 10000;
                        Tbelanja = belanja - 10000;
                    }
                    break;

                case "3":
                    if (belanja >= 100000)
                    {
                        Tongkir = ongkir - 20000;
                        Tbelanja = belanja - 10000;
                    }
                    break;
            }

            Console.WriteLine("Belanja:    "+belanja);
            Console.WriteLine("Ongkos Kirim:    " + ongkir);
            Console.WriteLine("Diskon Ongkir:    " + Tongkir);
            Console.WriteLine("Diskon Belanja:    " + Tbelanja);
            total = belanja + ongkir - Tongkir - Tbelanja;
            Console.WriteLine("Total Belanja:     "+total);



        }

        static void soal5()
        {
            string gen;
            Console.WriteLine("Masukan nama anda: ");
            string nama = Console.ReadLine();
            Console.WriteLine("Tahun berapa anda lahir? ");
            int lahir = int.Parse(Console.ReadLine());

            if ( lahir >= 1944 && lahir <= 1964)
            {
                 gen = "Baby Boomer";
            }else if ( lahir >= 1965 && lahir <= 1979)
            {
                gen = "Generasi X";
            }else if ( lahir >= 1980 && lahir <= 1994)
            {
                gen = "Generasi Y (Millenials)";
            }else if ( lahir >= 1995 && lahir <= 2015)
            {
                gen = "Generasi Z";
            }
            else
            {
                gen = "Tidak mempunyai generasi yang terdata";
            }

            Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong {gen}");


        }
    }
}

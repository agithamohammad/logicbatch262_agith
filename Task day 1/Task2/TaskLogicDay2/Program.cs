﻿using System;

namespace TaskLogicDay2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("Pilih soal 1/2/3/4/5: ");
                string soal = Console.ReadLine();

                switch (soal)
                {
                    case "1":
                        soal1();
                        break;

                    case "2":
                        soal2();
                        break;

                    case "3":
                        soal3();
                        break;

                    case "4":
                        soal4();
                        break;

                    case "5":
                        soal5();
                        break;

                    case "6":
                        soal6();
                        break;

                    case "7":
                        soal7();
                        break;

                    case "8":
                        soal8();
                        break;

                    case "9":
                        soal9();
                        break;

                }

                Console.WriteLine("Ulangi proses y/n ? ");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulangi = false;
                }

            }
        }

        static void soal1()
        {
            int a, b, n;
            n = 5;

            for (a = 1; a <= n; a++){
                for (b = 1; b <= a; b++)
                {
                    Console.Write("*");
                }
                Console.WriteLine("");
            }
        }

        static void soal2()
        {
            int a, b, c, n;
            n = 5;

            for (a = n; a >= 1; a--)
            {
                for (b = a; b >= 1; b--)
                {
                    Console.Write(" ");
                }
                for (c = n; c >= a; c--)
                {
                    Console.Write(c);
                }
                Console.WriteLine("");

            }
            
        }

        static void soal3()
        {
            //int x = 1, y = 1, z = 1;
            //Console.Write("Masukan jumlah bilangan fibonacci = "); 
            //int jumlah = Convert.ToInt32(Console.ReadLine());
            //Console.Write("1 ");
            //for (int i = 0; i < jumlah; i++) 
            //{ 
            //    Console.Write(z + " ");
            //    z = x + y; 
            //    x = y; 
            //    y = z; 
            //}
            //Console.Read();


            int a, b, n, c, d;
            n = 7;
            b = 1;
            c = 0;

            for (a = 1; a <= n; a++)
            {
                if (a <= 1)
                {
                    d = 1;
                }
                else
                {
                    d = b + c;
                    c = b;
                    b = d;
                }
                Console.WriteLine($"{d} ");
                
            }
        }

        static void soal4()
        {
            int a, b, n, c, d, e;
            n = 7;
            b = 1;
            c = 1;
            e = 1;

            for (a = 1; a <= n; a++)
            {
                if (a <= 3)
                {
                    d = 1;
                }
                else
                {
                    d = b + c + e;
                    c = b;
                    b = e;
                    e = d;
                }
                Console.WriteLine($"{d} ");
            }
        }

        static void soal5()
        {
            int a, b, n;
            n = 7;
            b = 1;
            
            for (a=2; a<=n; a++)
            {
                if (a % a == 0 || a % b == 0)
                {
                    b = a;
                    b = b - 1;
                    Console.WriteLine($"{a} ");
                }
            }


        }

        static void soal6()
        {
            int a, b;
            Console.WriteLine("Input : ");
            int input = int.Parse(Console.ReadLine());
            b = 1; 
            for (a=input; a>=1; a--)
            {
                if (a % 2 == 0)
                {

                    b = a / 2;
                    Console.WriteLine($"{a} / 2 = {b}");
                    b += 1;
                    a = b;
                }
                else if (a % 3 == 0)
                {
                    b = a / 3;
                    Console.WriteLine($"{a} / 3 = {b}");
                    b += 1;
                    a = b;
                    
                }
                else
                {
                    b = a / a;
                    Console.WriteLine($"{a} / {a} = {b}");
                }
            }
        }

        static void soal7()
        {
            Console.Write("Input nilai : ");
            string nilai = Console.ReadLine();
            string[] arrNilai = nilai.Split(",");
            int[] intNilai = Array.ConvertAll(arrNilai, int.Parse);

            Array.Sort(intNilai);
            Console.Write("Nilai yang di sort : ");

            foreach (int value in intNilai)
            {
                Console.Write($"{value} ");
            }
            Console.ReadKey();
            Console.Clear();
        }

        static void soal8()
        {
            //int a, b, n;
            //b = 3;

            int n, i, b;
            b = 1;
            n = 7;

            for ( i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write("* ");
                }
                else
                {
                    Console.Write($"{b} ");
                    b *= 3;
                }

            }
            Console.WriteLine("");

        }

        static void soal9()
        {
            int a, b, n;
            n = 7;
            b = 0;

            for (a = 1; a <= n; a++)
            {
                b += 5;

                if( b % 2 == 1)
                {
                    Console.Write($"-{b} ");
                }
                else
                {
                    Console.Write($"{b} ");
                }
            }
        }



    }
}

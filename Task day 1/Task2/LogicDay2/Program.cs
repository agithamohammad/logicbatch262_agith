﻿using System;

namespace LogicDay2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("Pilih soal 1/2/3/4/5: ");
                string soal = Console.ReadLine();

                switch (soal)
                {
                    case "1":
                        soal1();
                        break;

                    case "2":
                        soal2();
                        break;

                    case "3":
                        soal3();
                        break;

                    case "4":
                        soal4();
                        break;

                    case "5":
                        soal5();
                        break;

                    case "6":
                        soal6();
                        break;

                    case "7":
                        soal7();
                        break;

                    case "8":
                        soal8();
                        break;

                    case "9":
                        soal9();
                        break;

                    case "10":
                        soal10();
                        break;

                    case "11":
                        task1();
                        break;

                    case "12":
                        task2();
                        break;

                }

                Console.WriteLine("Ulangi proses y/n ? ");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulangi = false;
                }

            }
            
        }

        static void soal1()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 1;

            for (int i = 1; i <= n ; i++)
            {
                Console.Write($"{b} ");
                b += 2;
            }
            Console.WriteLine(""); 
        }

        static void soal2()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 2;

            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{b} ");
                b += 2;
            }
            Console.WriteLine("");
        }

        static void soal3()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 1;

            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{b} ");
                b += 3;
            }
            Console.WriteLine("");
        }

        static void soal4()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 1;

            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{b} ");
                b += 4;
            }
            Console.WriteLine("");
        }

        static void soal5()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 1;

            for (int i = 1; i <= n; i++)
            {
                if ( i % 3 == 0)
                {
                    Console.Write("* ");
                }
                else 
                {
                    Console.Write($"{b} ");
                    b += 4;
                }
                
            }
            Console.WriteLine("");
        }

        static void soal6()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 1;

            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write("* ");
                    b += 4;
                }
                else
                {
                    Console.Write($"{b} ");
                    b += 4;
                }

            }
            Console.WriteLine("");
        }

        static void soal7()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 2;

            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{b} ");
                b *= 2;
            }
            Console.WriteLine("");
        }

        static void soal8()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 3;

            for (int i = 1; i <= n; i++)
            {
                Console.Write($"{b} ");
                b *= 3;
            }
            Console.WriteLine("");
        }

        static void soal9()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 4;

            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write("* ");
                }
                else
                {
                    Console.Write($"{b} ");
                    b *= 4;
                }

            }
            Console.WriteLine("");
        }

        static void soal10()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 3;

            for (int i = 1; i <= n; i++)
            {
                if (i % 4 == 0)
                {
                    Console.Write("XXX ");
                    b *= 3;
                }
                else
                {
                    Console.Write($"{b} ");
                    b *= 3;
                }

            }
            Console.WriteLine("");
        }

        static void task1()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 1;

            for (int i = 1; i <= n; i++)
            {
                if (i % 7 == 0)
                {
                    Console.Write($"{n} ");
                }
                else
                {
                    Console.Write($"{b} ");
                    b *= 2;
                }

            }
            Console.WriteLine("");
        }

        static void task2()
        {
            Console.WriteLine("Masukan nilai : ");
            int n = int.Parse(Console.ReadLine());
            int b = 0;

            for (int i = 1; i <= n; i++)
            {
                if (b == 0)
                {
                    Console.Write("* ");
                    b += 2;
                }
                else if ( b % 3 == 0)
                {
                    Console.Write("* ");
                    b += 2;
                }
                else
                {
                    Console.Write($"{b} ");
                    b += 2;
                }

            }
            Console.WriteLine("");
        }
    }
}

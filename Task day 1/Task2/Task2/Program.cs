﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Silahkan pilih soal 1/2/3/4/5 : ");
            string pilih = Console.ReadLine();

            switch (pilih)
            {
                case "1":
                    soal1();
                    //soal1 s1 = new soal1();
                    //s1.soalnilai();
                    break;
                case "2":
                    soal2();
                    break;
                case "3":
                    soal3();
                    break;
                case "4":
                    soal4();
                    break;
                case "5":
                    soal5();
                    break;
                default:
                    break;
            }
            Console.ReadKey();
        }

        static void soal1()
        {
            float bmi, meter;
            string badan;
            badan = "";
            Console.WriteLine("Masukkan berat badan anda (KG): ");
            int berat = int.Parse(Console.ReadLine());
            Console.WriteLine("Masukkan tinggi badan anda (cm): ");
            int tinggi = int.Parse(Console.ReadLine());

            meter =(float) tinggi / 100;
            bmi =(float) berat / meter;

            if ( bmi < 18.5)
            {
                badan = "Kurus";
            }else if ( bmi >= 18.5 && bmi < 25)
            {
                badan = "Langsing/Sehat";
            }else if ( bmi > 25)
            {
                badan = "Gemuk";
            }

            Console.WriteLine($"{berat} & {meter}");
            Console.WriteLine($"Nilai BMI anda adalah {bmi}");
            Console.WriteLine($"Anda termasuk berbadan {badan}");
        }

        static void soal2()
        {
            int puntung, batang, harga, untung;
            puntung = 100;
            batang = puntung / 8;
            harga = 500;
            untung = batang * harga;

            Console.WriteLine($"Puntung rokok : {puntung}");
            Console.WriteLine($"{batang} batang rokok yang berhasil dirangkai dari {puntung} puntung rokok");
            Console.WriteLine($"Penghasilan dari {batang} batang rokok yang terjual : Rp. {untung}");

        }

        static void soal3()
        {
            int ratarata;

            Console.Write("Masukan nilai MTK : ");
            int mtk = int.Parse(Console.ReadLine());
            Console.Write("Masukan nilai Fisika : ");
            int fisika = int.Parse(Console.ReadLine());
            Console.Write("Masukan nilai Kimia : ");
            int kimia = int.Parse(Console.ReadLine());

            ratarata = (mtk + fisika + kimia) / 3;

            Console.WriteLine($"Nilai Rata-Rata : {ratarata}");

            if ( ratarata < 50)
            {
                Console.WriteLine("Maaf");
                Console.WriteLine("Kamu Gagal");
            }else if ( ratarata >= 50 && ratarata <= 100)
            {
                Console.WriteLine("Selamat");
                Console.WriteLine("Kamu Berhasil");
                Console.WriteLine("Kamu Hebat");
            }else
            {
                Console.WriteLine("Rata-Rata Not Found");
            }
        }

        static void soal4()
        {
            int upah, lembur, total, jamlembur, pajak, totalseluruh;
            upah = 0;
            lembur = 0;
            pajak = 0;
            totalseluruh = 0;
            Console.WriteLine("Golongan : ");
            string golongan = Console.ReadLine();
            Console.WriteLine("Jam Kerja : ");
            int jam = int.Parse(Console.ReadLine());

            switch (golongan)
            {
                case "1":
                    if (jam > 40)
                    {
                        upah = 40 * 2000;
                        jamlembur = jam - 40;
                        lembur = (int)(jamlembur * (2000 * 1.5));
                    }
                    else
                    {
                        upah = jam * 2000;
                    }
                    break;

                case "2":
                    if (jam > 40)
                    {
                        upah = 40 * 3000;
                        jamlembur = jam - 40;
                        lembur = (int)(jamlembur * (3000 * 1.5));
                    }
                    else
                    {
                        upah = jam * 3000;
                    }
                    break;

                case "3":
                    if (jam > 40)
                    {
                        upah = 40 * 4000;
                        jamlembur = jam - 40;
                        lembur = (int)(jamlembur * (4000 * 1.5));
                    }
                    else
                    {
                        upah = jam * 4000;
                    }
                    break;

                case "4":
                    if (jam > 40)
                    {
                        upah = 40 * 5000;
                        jamlembur = jam - 40;
                        lembur = (int)(jamlembur * (5000 * 1.5));
                    }
                    else
                    {
                        upah = jam * 5000;
                    }
                    break;
                default:
                    break;
            }

            total = upah + lembur;
            pajak = (int)(total * 0.05);
            totalseluruh = total + pajak;
            

            Console.WriteLine($"Upah : {upah}");
            Console.WriteLine($"Lembur : {lembur}");
            Console.WriteLine($"Total : {total}");
            Console.WriteLine($"Pajak : {pajak}");
            Console.WriteLine($"Total keseluruhan : {totalseluruh}");
        }

        static void soal5()
        {
            int harga;
            harga = 0;
            string merk;
            merk = "";
            Console.WriteLine("Masukan Kode Baju  = ");
            int baju = int.Parse(Console.ReadLine());
            Console.WriteLine("Masukan Kode Ukuran  = ");
            string ukuran = Console.ReadLine().ToUpper();
            

            switch (baju)
            {
                case 1:
                    merk = "IMP";
                    if ( ukuran == "S" )
                    {
                        harga = 200000;
                    }else if ( ukuran == "M" )
                    {
                        harga = 220000;
                    }
                    else
                    {
                        harga = 250000;
                    }
                    break;

                case 2:
                    merk = "Prada";
                    if (ukuran == "S")
                    {
                        harga = 150000;
                    }
                    else if (ukuran == "M")
                    {
                        harga = 160000;
                    }
                    else
                    {
                        harga = 170000;
                    }
                    break;

                case 3:
                    merk = "Gucci";
                    if (ukuran == "S")
                    {
                        harga = 200000;
                    }
                    else if (ukuran == "M")
                    {
                        harga = 200000;
                    }
                    else
                    {
                        harga = 200000;
                    }
                    break;

            }
            Console.WriteLine($"Merk Baju  = {merk}");
            Console.WriteLine($"Harga : {harga}");
        }




    }
}


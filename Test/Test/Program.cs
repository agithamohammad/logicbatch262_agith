﻿using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("Pilih soal 1-5 :  ");
                string soal = Console.ReadLine();

                switch (soal)
                {
                    case "1":
                        Class10 s1 = new Class10();
                        s1.soal1();
                        break;

                    case "2":
                        Class1 s2 = new Class1();
                        s2.soal2();
                        break;

                    case "3":
                        Class2 s3 = new Class2();
                        s3.soal3();
                        break;

                    case "4":
                        Class3 s4 = new Class3();
                        s4.soal4();
                        break;

                    case "5":
                        Class4 s5 = new Class4();
                        s5.soal5();
                        break;

                    case "6":
                        Class5 s6 = new Class5();
                        s6.soal6();
                        break;

                    case "7":
                        Class6 s7 = new Class6();
                        s7.soal7();
                        break;

                    case "8":
                        Class7 s8 = new Class7();
                        s8.soal8();
                        break;

                    case "9":
                        Class8 s9 = new Class8();
                        s9.soal9();
                        break;

                    case "10":
                        Class9 s10 = new Class9();
                        s10.soal10();
                        break;
                }

                Console.WriteLine("Ulangi proses y/n ? ");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulangi = false;
                }
            }
        }


    }
}

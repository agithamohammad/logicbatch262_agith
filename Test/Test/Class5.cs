﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    class Class5
    {
        public void soal6()
        {
            Console.WriteLine("Input : ");
            int max = int.Parse(Console.ReadLine());
            for (int a = 0; a < max; a+=3)
            {
                if (a % 2 == 0)
                {
                    Console.WriteLine($"{a} Merupakan Bilangan Genap");
                }
            }
        }

    }
}

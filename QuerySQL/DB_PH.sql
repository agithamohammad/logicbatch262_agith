--13. Menampilkan nama film yang artisnya bukan berasal dari negara yang tidak mengandung huruf 'o'
--14. Menampilkan nama artis yang tidak pernah bermain film 
--15. Menampilkan nama artis yang bermain film dengan genre drama 

--13
select nm_film 
from tb_film as tf
join artis as ta
on tf.artis = ta.kd_artis
join tb_negara as tn
on ta.negara = tn.kd_negara
where nm_negara not like '%o%'

--14
select nm_artis 
from tb_film as tf
right join artis as ta 
on tf.artis=ta.kd_artis
where artis is null

--15
select nm_artis 
from artis as ta join 
tb_film as tf
on ta.kd_artis = tf.artis
join tb_genre as tg
on tf.genre = tg.kd_genre
where nm_genre = 'DRAMA'

--16
select nm_artis 
from artis as ta join 
tb_film as tf
on ta.kd_artis = tf.artis
join tb_genre as tg
on tf.genre = tg.kd_genre
where nm_genre = 'ACTION'
group by nm_artis

--17
select nm_negara, count(nm_negara) as juml_film
from tb_negara as tn 
join artis as ta
on tn.kd_negara = ta.negara
join tb_film as tf
on ta.kd_artis = tf.artis
group by nm_negara

select nm_film
from tb_film as tf
join tb_produser as tp
on tf.produser = tp.kd_produser
where international = 'YA'

--19
select nm_produser, count(nm_film) as juml_film
from tb_produser as tp
join tb_film as tf
on tp.kd_produser = tf.produser
group by nm_produser

--20
select nm_produser, sum(pendapatan) as juml_pendapatan
from tb_produser as tp
join tb_film as tf
on tp.kd_produser = tf.produser
group by nm_produser
having nm_produser = 'MARVEL'

--create database DBPenerbit

use DBPenerbit

--create table tblPengarang(
--id int primary key identity (1,1),
--Kd_Pengarang varchar(7) not null,
--Nama varchar(30) not null,
--Alamat varchar(80) not null,
--Kota varchar(15) not null,
--Kelamin varchar(1) not null,)

--insert into tblPengarang (Kd_Pengarang, Nama, Alamat, Kota, Kelamin)
--values
----('P0001','Ashadi','Jl.Beo 25','Yogya','P')
--('P0002','Rian','Jl.Solo 123','Yogya','P'),
--('P0003','Suwadi','Jl.Semangka 13','Bandung','P'),
--('P0004','Siti','Jl.Durian 15','Solo','W'),
--('P0005','Amir','Jl.Gajah 33','Kudus','P'),
--('P0006','Suparman','Jl.Harimau 25','Jakarta','P'),
--('P0007','Jaja','Jl.Singa 7','Bandung','P'),
--('P0008','Saman','Jl.Naga 12','Yogya','P'),
--('P0009','Anwar','Jl.Tidar 6A','Magelang','P'),
--('P0010','Fatmawati','Jl.Renjana 4','Bogor','W')

create table tblGaji(
id int primary key identity(1,1),
Kd_Pengarang varchar(7) not null,
Nama varchar(30) not null,
Gaji decimal(18,4) not null)

insert into tblGaji(Kd_Pengarang, Nama, Gaji)
values
('P0002','Rian',600000),
('P0005','Amir',700000),
('P0004','Siti',500000),
('P0003','Suwadi',1000000),
('P0010','Fatmawati',600000),
('P0008','Saman',750000)

--No.1
select count(*) as jml_pengarang from tblPengarang

--No.2
select Kelamin, count(*) as jml_Pengarang from tblPengarang group by Kelamin

--No.3
select Kota,count(*) as jml_kota from tblPengarang group by Kota

--No.4
select Kota,count(kota) as jml_kota from tblPengarang group by Kota having count (Kota) > 1


--No.5
select Kd_Pengarang from tblPengarang order by Kd_Pengarang desc

--No.6
select min(Gaji) as Gaji_Tertinggi, max(Gaji) as Gaji_Terendah from tblGaji 

--No.7
select Gaji from tblGaji where Gaji > 600000 order by Gaji asc

--No.8
select sum(gaji) from tblGaji

--No.9
select Kota,Gaji from tblPengarang as P inner join tblGaji as G on
P.Kd_Pengarang = G.Kd_Pengarang

--No.10
select * from tblPengarang where Kd_Pengarang between 'P0001' and 'P0006'

--No.11
select * from tblPengarang where Kota in ('Yogya','Solo','Magelang')

--No.12
select * from tblPengarang where Kota not in ('Yogya')

--No.13 A-D
select * from tblPengarang where Nama like 'A%' 
select * from tblPengarang where Nama like '%i' 
select * from tblPengarang where Nama like '%a%'
select * from tblPengarang where Nama not like '%n' 

--No.14
select * from tblPengarang as P inner join tblGaji as g on p.Kd_Pengarang = g.Kd_Pengarang

--No.15
select Kota from tblPengarang as P inner join tblGaji as g on p.Kd_Pengarang = g.Kd_Pengarang where Gaji < 1000000

--No.16
alter table tblPengarang alter column Kelamin varchar(10);

--No.17
alter table tblPengarang add Gelar varchar(12) 

--No.18
update dbo.tblPengarang set Alamat = 'Jl.Renjana 4', Kota = 'Bogor' where Kd_Pengarang = 'P0010'

--No.19
create view vwPengarang as
select Kd_Pengarang,Nama,Kota from tblPengarang 



--10.    Tampilkan seluruh record pengarang antara P0001-P0006 dari tabel pengarang.
--11.    Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang.
--12.    Tampilkan seluruh data yang bukan yogya dari tabel pengarang.

select * from tblPengarang where Kd_Pengarang between 'P0001' and'P0006'

select *  from tblPengarang where Kota in ('Yogya','Solo','Magelang')

select * from tblPengarang where Kota not in ('Yogya')
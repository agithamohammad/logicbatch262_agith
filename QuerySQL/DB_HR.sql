create database DB_HR

create table tb_divisi (
id bigint primary key identity(1,1),
kd_divisi varchar (50) not null,
nama_divisi varchar (50) not null)

create table tb_jabatan (
id bigint primary key identity(1,1),
kd_jabatan varchar (50) not null,
nama_jabatan varchar (50) not null,
gaji_pokok int not null,
tunjangan_jabatan int not null)

create table tb_karyawan (
id bigint primary key identity(1,1),
nip varchar (50) not null,
nama_depan varchar (50) not null,
nama_belakang varchar (50) not null,
jenis_kelamin varchar (50) not null,
agama varchar (50) not null,
tempat_lahir varchar(50) not null,
tgl_lahir date not null,
alamat varchar (100) not null,
pendidikan_terakhir varchar (50) not null,
tgl_masuk date not null)

create table tb_pekerjaan (
id bigint primary key identity(1,1),
nip varchar (50) not null,
kode_jabatan varchar (50) not null,
kode_divisi varchar (50) not null,
tunjangan_kinerja int not null,
kota_penempatan varchar (50) not null,)

insert into tb_divisi (
kd_divisi,nama_divisi)
values
('GD','Gudang'),
('HRD','HRD'),
('KU','Keuangan'),
('UM','Umum')

insert into tb_jabatan (
kd_jabatan,nama_jabatan,gaji_pokok,tunjangan_jabatan)
values
('MGR','Manager',5500000,1500000),
('OB','Office Boy',1900000,200000),
('ST','Staff',3000000,750000),
('WMGR','Wakil Manager',4000000,1200000)

insert into tb_karyawan(
nip,nama_depan,nama_belakang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,pendidikan_terakhir,tgl_masuk)
values
('001','Hamidi','Samsudin','Pria','Islam','Sukabumi','1997-04-21','Jl.Sudirman No.12','S1 Teknik Mesin','2015-12-07'),
('003','Paul','Christian','Pria','Kristen','Ambon','1980-05-27','Jl.Veteran No.4','S1 Pendidikan Geografi','2014-01-12'),
('002','Ghandi','Wamida','Pria','Islam','Palu','1992-01-12','Jl.Rambutan No.22','SMA Negeri 02 Palu','2014-12-01')

insert into tb_pekerjaan(
nip,kode_jabatan,kode_divisi,tunjangan_kinerja,kota_penempatan)
values 
('001','ST','KU',750000,'Cianjur'),
('002','OB','UM',350000,'Sukabumi'),
('003','MGR','HRD',1500000,'Sukabumi')

--1
select (nama_depan + nama_belakang) as nama_lengkap, nama_jabatan,(gaji_pokok + tunjangan_jabatan) as gaji_tunjangan 
from tb_karyawan as tk 
join tb_pekerjaan as tp on tk.nip = tp.nip 
join tb_jabatan as tj on tp.kode_jabatan = tj.kd_jabatan
where (gaji_pokok + tunjangan_jabatan) < 5000000;

--2
select (nama_depan + nama_belakang) as nama_lengkap, 
nama_jabatan, nama_divisi, (gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) as total_gaji,
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) * 0.05 as pajak, 
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) - (gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) * 0.05  as gaji_bersih
from tb_karyawan as tk 
join tb_pekerjaan as tp on tk.nip = tp.nip 
join tb_jabatan as tj on tp.kode_jabatan = tj.kd_jabatan 
join tb_divisi as td on tp.kode_divisi = td.kd_divisi
where jenis_kelamin = 'Pria' and kota_penempatan not in ('Sukabumi')
--group by nama_depan, nama_belakang, nama_jabatan, nama_divisi, gaji_pokok, tunjangan_jabatan, tunjangan_kinerja


--3
select tk.nip, (nama_depan + nama_belakang) as nama_lengkap, nama_jabatan, nama_divisi, (((gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) * 7) * 0.25) as bonus
from tb_karyawan as tk join tb_pekerjaan as tp on tk.nip = tp.nip join tb_jabatan as tj on tp.kode_jabatan = tj.kd_jabatan join tb_divisi as td on
tp.kode_divisi = td.kd_divisi
--group by tk.nip, nama_depan, nama_belakang, nama_jabatan, nama_divisi, gaji_pokok, tunjangan_jabatan, tunjangan_kinerja

--4
select (nama_depan + nama_belakang) as nama_lengkap, nama_jabatan, nama_divisi, 
(gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) * 0.05 as infak from tb_karyawan as tk join tb_pekerjaan as tp on tk.nip = tp.nip join tb_jabatan
as tj on tp.kode_jabatan = tj.kd_jabatan join tb_divisi as td on tp.kode_divisi = td.kd_divisi
where kode_jabatan = 'MGR'
--group by nama_depan, nama_belakang, nama_jabatan, nama_divisi, gaji_pokok, tunjangan_jabatan, tunjangan_kinerja

--5
select (nama_depan + nama_belakang) as nama_lengkap, nama_jabatan, pendidikan_terakhir,2000000 as tunjangan_pendidikan, (gaji_pokok + tunjangan_jabatan + 2000000) as total_gaji
from tb_karyawan as tk join tb_pekerjaan as tp on tk.nip = tp.nip join tb_jabatan as tj on tp.kode_jabatan = tj.kd_jabatan 
join tb_divisi as td on tp.kode_divisi = td.kd_divisi
where pendidikan_terakhir LIKE 'S1%'

--6
select tk.nip, (nama_depan + nama_belakang) as nama_lengkap, nama_jabatan, nama_divisi,
case
	when kode_jabatan = 'MGR' then ((gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) * 7 ) * 0.25
	when kode_jabatan = 'ST' then ((gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) * 5 ) * 0.25
	when kode_jabatan != 'MGR' and kode_jabatan != 'ST' then ((gaji_pokok + tunjangan_jabatan + tunjangan_kinerja) * 2 ) * 0.25
	end bonus
from tb_karyawan as tk join tb_pekerjaan as tp on tk.nip = tp.nip join tb_jabatan as tj on tp.kode_jabatan = tj.kd_jabatan 
join tb_divisi as td on tp.kode_divisi = td.kd_divisi
--group by tk.nip, nama_depan, nama_belakang, nama_jabatan, nama_divisi, gaji_pokok, tunjangan_jabatan, tunjangan_kinerja


--7
alter table tb_karyawan
add constraint uc_karyawan unique (nip);

--8
create unique index  index_nip
on tb_karyawan (nip);

--9
select  nama_depan+' '+ upper (nama_belakang) as nama_lengkap
from tb_karyawan where nama_belakang like 'W%'


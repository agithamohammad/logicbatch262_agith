--create database DBKaryawan

use DBKaryawan

create table Karyawan ( 
id bigint primary key identity(1,1),
nomor_induk varchar(7) not null,
nama varchar (30) not null,
alamat text not null,
tanggal_lahir date not null,
tanggal_masuk date not null)

alter table Karyawan
add primary key (nomor_induk)

insert into Karyawan(nomor_induk,nama,alamat,tanggal_lahir,tanggal_masuk)
values
('IP06001','Agus','Jln.Gajah Mada 115A, Jakarta Pusat','8/1/70','7/7/06'),
('IP06002','Amin','Jln.Bungur Sari V No.178, Bandung','5/3/77','7/6/06'),
('IP06003','Yusuf','Jln.Yosodpuro 15, Surabaya','8/9/73','7/8/06'),
('IP07004','Alyssa','Jln.Cendana No.6, Bandung','2/14/83','1/5/07'),
('IP07005','Maulana','Jln.Ampera Raya No.1','10/10/85','2/5/07'),
('IP07006','Afika','Jln.Pejaten Barat No.6A','3/9/87','6/9/07'),
('IP07007','James','Jln.Padjadjaran No.111, Bandung ','5/19/88','6/9/06'),
('IP09008','Octavanus','Jln.Gajah Mada 101. Semarang','10/7/88','8/8/08'),
('IP09009','Nugroho','Jln.Duren Tiga 196, Jakarta Selatan','1/20/88','11/11/08'),
('IP09010','Raisa','Jln.Nangka Jakarta Selatan','12/29/89','2/9/09')

create table cuti_karyawan(
id bigint primary key identity(1,1),
nomor_induk varchar (7) not null,
tanggal_mulai date not null,
lama_cuti int not null,
keterangan text not null)

alter table cuti_karyawan
alter column lama_cuti smallint

insert into cuti_karyawan
(nomor_induk,tanggal_mulai,lama_cuti,keterangan)
values
('IP06001','2/1/12','3','Acara keluar'),
('IP06001','2/13/12','4','Anak sakit'),
('IP07007','2/15/12','2','Nenek sakit'),
('IP06003','2/17/12','1','Mendaftar sekolah anak'),
('IP07006','2/20/12','5','Menikah'),
('IP07004','2/27/12','1','Imunisasi anak')


--1
select top 3 nama from Karyawan
order by tanggal_masuk

--2
select k.nomor_induk, nama, tanggal_mulai, lama_cuti, keterangan
from Karyawan as k
join  cuti_karyawan as ck
on k.nomor_induk = ck.nomor_induk
where DATEADD(day,lama_cuti,tanggal_mulai)>'2012-02-16' 
and tanggal_mulai < '2012-02-16'

--select dateadd(day, lama_cuti, tanggal_mulai) as tanggal_kembali 
--from cuti_karyawan
--where

--3
select ck.nomor_induk, nama, count(lama_cuti) as lama_cuti
from Karyawan as k
join  cuti_karyawan as ck
on k.nomor_induk = ck.nomor_induk
where  DATEDIFF (day,tanggal_mulai,'2012-02-16')>=1
group by ck.nomor_induk, nama

--4
select k.nomor_induk, nama,
case 
	when sum(ck.lama_cuti) != 0 then 12-sum(ck.lama_cuti)
	else'12'
	end sisa_cuti
from Karyawan as k
left join cuti_karyawan as ck
on k.nomor_induk = ck.nomor_induk
group by k.nomor_induk, nama


select ck.nomor_induk, nama,tanggal_mulai,lama_cuti,DATEADD(day,lama_cuti,tanggal_mulai) as tanggal_selesai,'2012-02-16' as tgl_skrg,
case
	when DATEADD(day,lama_cuti,tanggal_mulai) < '2012-02-16' then 0
	when DATEDIFF(day, '2012-02-16', tanggal_mulai ) > 1 then lama_cuti
	when DATEDIFF(day, '2012-02-16', tanggal_mulai ) > 12 then 12
	when DATEADD(day,lama_cuti,tanggal_mulai) > '2012-02-16' then lama_cuti - DATEDIFF(day, tanggal_mulai,'2012-02-16')
	when tanggal_mulai > '2012-02-16' then lama_cuti
	--when DATEADD(day,lama_cuti,tanggal_mulai) > '2012-02-16' then 
	end sisa_cuti
from Karyawan as k
join  cuti_karyawan as ck
on k.nomor_induk = ck.nomor_induk



--create database DBUniv

use DBUniv

create table Mahasiswa(
id int primary key identity (1,1),
Nim varchar(20) not null,
nama varchar(30) not null,
jenis_kelamin varchar(1)not null,
alamat varchar(80)not null)

alter table Mahasiswa alter column Kelamin varchar(10);

insert into Mahasiswa (Nim,nama,jenis_kelamin,alamat)
values
('101','Arif','L','Jl.Kenangan'),
('102','Budi','L','Jl.Jombang'),
('103','Wati','L','Jl.Surabaya'),
('104','Ika','L','Jl.Jombang'),
('105','Tono','L','Jl.Jakarta'),
('106','Iwan','L','Jl.Bandung'),
('107','Sari','L','Jl.Malang')

create table ambil_mk(
id int primary key identity (1,1),
Nim varchar(20) not null,
kode_mk varchar(30) not null)

insert into ambil_mk (Nim,kode_mk)
values
('101','PTI447'),
('103','TIK333'),
('104','PTI333'),
('104','PTI777'),
('111','PTI123'),
('123','PTI999')

create table Matakuliah(
id int primary key identity (1,1),
kode_mk varchar(30) not null,
nama_mk varchar(30) not null,
sks varchar(5) not null,
semester varchar(5) not null)

insert into Matakuliah(kode_mk, nama_mk, sks, semester)
values
('PTI447','Praktikum Basis Data','1','3'),
('PTI342','Praktikum Basis Data','1','3'),
('PTI333','Basis Data Terdistribusi','3','5'),
('TIK123','Jaringan Komputer','2','5'),
('TIK333','Sistem Operasi','3','5'),
('PTI123','Grafika Multi media ','3','5'),
('PTI777','Sistem Informasi','2','3')
			
alter table Matakuliah alter column sks int;

SELECT tblPegawai.Nama, tblPegawai.NIP, tblPegawai.Unit, tblJabatan.Jabatan, tblGolongan.golongan 
FROM tblPegawai JOIN tblJabatan ON 
tblPegawai.Nama = tblJabatan.NamaPegawai JOIN tblGolongan ON tblPegawai.Nama = tblGolongan.NamaPegawai

--1
select nama,nama_mk from Mahasiswa as m inner join ambil_mk as am on m.Nim = am.Nim
inner join Matakuliah as mk on am.kode_mk = mk.kode_mk
--2
select nama from Mahasiswa as m left join ambil_mk as am on m.Nim = am.Nim
left join Matakuliah as mk on am.kode_mk = mk.kode_mk where nama_mk is null
--3
select mhs.jenis_Kelamin, count(mhs.jenis_Kelamin) as jumlah from Mahasiswa as mhs
left join ambil_mk as mk
on mhs.NIM = mk.Nim
where mk.kode_mk is null
group by mhs.jenis_kelamin
--4
select m.nim,m.nama,am.kode_mk,mk.nama_mk from Mahasiswa as m inner join ambil_mk as am on m.Nim = am.Nim
inner join Matakuliah as mk on am.kode_mk = mk.kode_mk

--5
select m.nim,m.nama,sum(mk.sks) as total from ambil_mk as am join Mahasiswa as m on m.Nim = am.Nim join
Matakuliah as mk on am.kode_mk = mk.kode_mk group by m.Nama,m.Nim having sum(sks)>4 and sum(sks)<10;

--6
select nama_mk from Matakuliah as mk left join ambil_mk as am on mk.kode_mk = am.kode_mk
left join Mahasiswa as m on m.Nim = am.nim where nama is null


create database DB_PH

use DB_PH

create table artis (
id bigint identity(1, 1),
kd_artis varchar (100) not null,
nm_artis varchar (100) not null,
jk varchar (100) not null,
bayaran int not null,
award int not null,
negara varchar (100))

alter table  artis
add primary key (kd_artis);

create table tb_negara (
id bigint identity(1, 1),
kd_negara varchar (100) not null,
nm_negara varchar (100) not null)

alter table  tb_negara
add primary key (kd_negara);

create table tb_genre (
id bigint identity(1, 1),
kd_genre varchar (100) not null,
nm_genre varchar (100) not null)

alter table  tb_genre
add primary key (kd_genre);

create table tb_produser (
id bigint identity(1, 1),
kd_produser varchar (50) not null,
nm_produser varchar (50) not null,
international varchar (50) not null)

alter table  tb_produser
add primary key (kd_produser);

create table tb_film (
id bigint identity(1, 1),
kd_film varchar (10) not null,
nm_film varchar (55) not null,
genre varchar (55) not null,
artis varchar (55) not null,
produser varchar (55) not null,
pendapatan int not null,
nominasi int not null)

alter table  tb_film
add primary key (kd_film);

insert into artis(kd_artis, nm_artis, jk, bayaran, award, negara)
values
('A001','ROBERT DOWNEY JR','PRIA',2000000000,2,'AS'),
('A002','ANGELINA JOLIE','WANITA',700000000,1,'AS'),
('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
('A005','CHELSEA ISLAN','WANITA',300000000,0,'ID')

insert into tb_negara(kd_negara, nm_negara)
values
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

insert into tb_genre(kd_genre, nm_genre)
values
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

insert into tb_produser(kd_produser, nm_produser, international)
values
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')

insert into tb_film(kd_film, nm_film, genre, artis, produser, pendapatan, nominasi)
values
('F001','IRON MAN','G001','A001','PD01',2000000000,3),
('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
('F004','AVENGER:CIVIL WAR','G001','A001','PD01',2000000000,1),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
('F006','THE RAID','G001','A004','PD03',800000000,5),
('F007','FAST & FURIOUS','G001','A004','PD05',830000000,2),
('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
('F009','POLICE STORY','G001','A003','PD02',700000000,3),
('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
('F013','KUNGFU PANDA','G003','A003','PD05',923000000,5)

--1
select nm_film, nominasi 
from tb_film
order by nominasi desc

--2
select nm_film ,nominasi
from tb_film
where nominasi = (select max(nominasi) from tb_film)
--group by nominasi

select nm_film, max(nominasi)
from tb_film
group by nm_film,nominasi
having max(nominasi) >= nominasi

select top 2 nm_film, nominasi
from tb_film
order by nominasi desc

--3
select nm_film, nominasi 
from tb_film 
where nominasi = 0

--4
select nm_film, pendapatan
from tb_film
order by pendapatan 

--5
select nm_film, pendapatan
from tb_film
where pendapatan = (select max(pendapatan) from tb_film)

--6
select nm_film 
from tb_film
where nm_film like 'p%'

--7
select nm_film 
from tb_film
where nm_film like '%y'

--8
select nm_film 
from tb_film
where nm_film like '%d%'
 
--9
select nm_film
from tb_film
where pendapatan

--10
select top 1 nm_film
from tb_film
where nm_film like '%o%'
order by pendapatan

--11
select nm_film, nm_artis 
from tb_film as tf 
join artis as ta 
on tf.artis = ta.kd_artis

--12
select nm_film, nm_artis 
from tb_film as tf 
join artis as ta 
on tf.artis = ta.kd_artis
where negara in ('HK')

--13
select nm_film
from tb_film as tf
join artis as ta
on tf.artis = ta.kd_artis
join tb_negara as tn
on ta.negara = tn.kd_negara
where nm_negara not like '%o%'

--14
select nm_artis
from tb_film as tf
right join artis as ta
on tf.artis = ta.kd_artis
where artis is null

--15
select nm_artis
from artis as ta
join tb_film as tf
on ta.kd_artis = tf.artis
join tb_genre as tg
on tf.genre = tg.kd_genre
where nm_genre = 'DRAMA'

--16

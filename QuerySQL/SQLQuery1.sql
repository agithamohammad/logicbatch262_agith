--Create database Batch2

use Batch262
--create table peserta ( 
--id int primary key identity (1,1),
--name varchar(50) not null,
--address varchar(100) not null,
--email varchar(30) null,
--gender varchar(15) null,)

--create table penilaian ( 
--id int primary key identity (1,1),
--id_peserta int not null,
--nilai int not null)

--insert into penilaian (id_peserta, nilai)
--values
--(1,70),(2,80),(3,84),(4,89)

--select * from penilaian

--update penilaian
--set nilai = 80
--where nilai = 70

insert into peserta (name, address, email, gender)
values
('Deby','Jakarta Barat','deby@gmail.com','wanita'),
('Hendri','Jakarta Timur','hendri@gmail.com','pria'),
('Made','Jakarta Selatan','made@gmail.com','wanita'),
('Sopan','Jakarta Barat','sopan@gmail.com','wanita')

select * from peserta as p
join penilaian as n
on p.id = n.id_peserta
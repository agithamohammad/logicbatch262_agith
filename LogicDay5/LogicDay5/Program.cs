﻿using System;

namespace LogicDay5
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("Pilih soal 1-5 :  ");
                string soal = Console.ReadLine();

                switch (soal)
                {
                    case "1":
                        soal1();
                        break;

                    case "2":
                        soal2();
                        break;

                    case "3":
                        soal3();
                        break;

                        //case "4":
                        //    soal4();
                        //    break;

                        //case "5":
                        //    soal5();
                        //    break;

                }

                Console.WriteLine("Ulangi proses y/n ? ");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulangi = false;
                }

            }
        }

        static void soal1()
        {

            Console.Write("Input Panjang : ");
            string input = Console.ReadLine();
            string[] inputt = input.Split(" ");
            int[] arrInput = Array.ConvertAll(inputt, int.Parse);

            Console.WriteLine("Input Text : ");
            string text = Console.ReadLine();

            string huruf = "abcdefghijklmnopqrstuvwxyz";
            char[] arrHuruf = huruf.ToCharArray();
            int temp = 0;

            for (int i = 0; i < text.Length; i++)
            {
                int index = Array.IndexOf(arrHuruf, text[i]);
                int elementTinggi = arrInput[index];
                
                
                if(elementTinggi > temp)
                {
                    temp = elementTinggi;
                }          
            }
            Console.WriteLine($"Tertinggi = {temp}");
            Console.WriteLine($"Panjang Text = {text.Length}");
            int jawab = temp * text.Length;
            Console.WriteLine($"{temp} x {text.Length} = {jawab}" );
        }

        static void soal2()
        {

            Console.Write("Point : ");
            int poin = int.Parse(Console.ReadLine());

            //bool ulangi = true;
            int sisapoin = 0;

            //while (ulangi)
            //{
            //    poin = sisapoin;

                Console.Write("Taruhan : ");
                int taruhan = int.Parse(Console.ReadLine());

                Console.Write("Tebakan (U/D) : ");
                string tebak = Console.ReadLine().ToUpper();

                Random rnd = new Random();
                int angka = rnd.Next(0, 10);


                if (angka <= 5 && tebak == "D")
                {

                    Console.WriteLine("You Win");
                    sisapoin = poin + taruhan;
                    Console.WriteLine(angka);
                    Console.WriteLine($"Poin saat ini : {sisapoin}");

                }
                else if (angka > 5 && tebak == "U")
                {

                    Console.WriteLine("You Win");
                    sisapoin = poin + taruhan;
                    Console.WriteLine(angka);
                    Console.WriteLine($"Poin saat ini : {sisapoin}");
                }
                else
                {

                    Console.WriteLine("You Lose");
                    sisapoin = poin - taruhan;
                    Console.WriteLine(angka);
                    Console.WriteLine($"Poin saat ini : {sisapoin}");
                }

                


                //if (sisapoin <= 0)
                //{
                //    Console.WriteLine($"Poin saat ini : {sisapoin}");
                //    Console.WriteLine("Anda Bangkrut!");
                //    //ulangi = false;
                //    //break;
                //}

                //Console.WriteLine("Main lagi (y/n) : ");
                //string main = Console.ReadLine();

                //if (main.ToLower() == "n")
                //{
                //    ulangi = false;
                //}
            


        }

        static void soal3()
        {

            Console.WriteLine("Masukan Nilai : ");
            int nilai = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan Huruf : ");
            string huruf = Console.ReadLine().ToUpper();
            char[] arrHuruf = huruf.ToCharArray();


            int x;
            int y = 0;
            int z = 1;
            int b = 1;

            for (int a =1; a<=b; a++) { 

            if ( nilai == huruf.Length)
            {
                continue;
            }
            else if ( nilai < huruf.Length)
            {
                Console.WriteLine("Huruf yang anda masukan lebih");
                break;
            }
            else
            {
                Console.WriteLine("Huruf yang anda masukan kurang");
                break;
            }
            }

            //int temp = 0;

            for(int i = 0; i < huruf.Length; i++)
            {
                if(arrHuruf[i] == 'D')
                {
                    x = -1;
                    y = z + x;
                    z = y;
                }
                else if( huruf [i] == 'U')
                {
                    x = 1;
                    y = z + x;
                    z = y;
                }
            }

            Console.WriteLine(z);




        }
    }
}

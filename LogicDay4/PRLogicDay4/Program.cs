﻿using System;

namespace PRLogicDay4
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("Pilih soal 1-5 :  ");
                string soal = Console.ReadLine();

                switch (soal)
                {
                    case "1":
                        soal1();
                        break;

                    case "2":
                        soal2();
                        break;

                    case "3":
                        soal3();
                        break;

                    //case "4":
                    //    soal4();
                    //    break;

                    //case "5":
                    //    soal5();
                    //    break;

                }

                Console.WriteLine("Ulangi proses y/n ? ");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulangi = false;
                }

            }

        }

        static void soal1()
        {
            string huruf = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            char[] arrHuruf = huruf.ToCharArray();

            Console.WriteLine("masukan sandi/password : ");
            string password = Console.ReadLine();

            Console.WriteLine("Masukan rotasi : ");
            int rotasi = int.Parse(Console.ReadLine());

            string encryptPassword = "";

            for (int i = 0; i < password.Length; i++)
            {
                char kata = password[i];
                int index = Array.IndexOf(arrHuruf, kata);

                if (index != -1)
                {
                    encryptPassword += arrHuruf[index + rotasi];
                }
                else
                {
                    encryptPassword += password[i];
                }
            }
            Console.WriteLine($"{encryptPassword}");




        }

        static void soal2()
        {
            Console.Write("masukkan input ");
            int input = int.Parse(Console.ReadLine());
            int angka = 0;
            int baris = 3;
            for (int i = 0; i < (input * baris); i++)
            {
                Console.Write(angka + " ");
                angka++;
                if (angka % input == 0)
                {
                    Console.WriteLine();
                }
            }

            //int a, b, c, n;
            //n = 7;
            //c = 20;

            //for(a=1; a < 3; a++)
            //{
            //    for(b=1; b <= n; b++)
            //    {
            //        if ()
            //        Console.Write(b);
            //    }
            //    Console.WriteLine("");
            //}
        }

        static void soal3()
        {
            Console.WriteLine("Pusing, Tapi aku harus bisa!");

        }
    }
}

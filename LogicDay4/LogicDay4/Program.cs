﻿using System;

namespace LogicDay4
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("Pilih soal 1-5 :  ");
                string soal = Console.ReadLine();

                switch (soal)
                {
                    case "1":
                        soal1();
                        break;

                    case "2":
                        soal2();
                        break;

                    case "3":
                        soal3();
                        break;

                    case "4":
                        soal4();
                        break;

                    case "5":
                        soal5();
                        break;

                }

                Console.WriteLine("Ulangi proses y/n ? ");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulangi = false;
                }

            }
        }

        static void soal1()
        {
            string jam = "07:05:45";
            string[] strIndex = jam.Split(":");
            int[] arrIndex = Array.ConvertAll(strIndex, int.Parse);

            string strJam = jam.Substring(0, 2);

            int j = Convert.ToInt32(strJam);

            //int intJam = int.Parse(strJam);
            int a = j + 12;
            string hasil = a.ToString();

            Console.WriteLine($"{a} {jam}");

            for (int i = 0; i < arrIndex.Length; i++)
            {
                if (arrIndex[i] == 0)
                {
                    arrIndex[i] = a;
                    Console.WriteLine(arrIndex[i]);
                }


            }

            Console.WriteLine($"{a} {jam}");



            //Console.Write("Input String: ");

            //string sos = Console.ReadLine().ToLower();
            //int panjang = sos.Length;
            //string[] tampung = new string[panjang];

            //int countF = 0;

            //for (int i = 0; i < panjang; i = i + 3)
            //{
            //    string pecah = sos.Substring(i, 3);
            //    tampung[i] = pecah;

            //    if (tampung[i] == "sos")
            //    {
            //        continue;
            //    }
            //    else
            //    {
            //        countF = countF + 1;
            //    }
            //}

        }

        static void soal2()
        {

            //Console.Write("Input: ");
            //string index = Console.ReadLine();
            //string[] strIndex = index.Split(" ");
            //int[] arrIndex = Array.ConvertAll(strIndex, int.Parse);

            //double[] jarak = { 2, 0.5, 1.5, 0.3 };

            //double jaraktempuh = 0;
            ////double totalJarakTempuh;

            //for (int i = 0; i < arrIndex.Length; i++)
            //{
            //    int idx = arrIndex[i] - 1;
            //    jaraktempuh += jarak[idx];
            //    Console.Write($"{jarak[idx]}KM ");
            //}

            //Console.WriteLine($"= {jaraktempuh}KM ");


            Console.Write("Input: ");
            string input = Console.ReadLine();
            string[] strInput = input.Split(" ");
            int[] arrInput = Array.ConvertAll(strInput, int.Parse);

            double[] jarak = { 2, 0.5, 1.5, 0.3 };

            double jaraktempuh = 0;
            int bensin = 1;

            for (int i = 0; i < arrInput.Length; i++)
            {
                int j = arrInput[i] - 1;
                jaraktempuh += jarak[j];
                Console.Write($"{jarak[j]}KM ");

                if (i == 0 || i <= arrInput.Length - 2)
                {
                    Console.Write("+ ");
                }
            }

            if (jaraktempuh > 2.5)
            {
                bensin++;
            }

            Console.WriteLine($"= {jaraktempuh}KM ");
            Console.WriteLine($"Bensin = {bensin} Liter");



            //float[] fJarak = new float[intInput.Length];

            //int meter, bensin;
            //meter = 0;
            //float jarak = 0;

            //for ( int a = 1; a <= intInput.Length ; a++)
            //{


            //    if (intInput[a] == 1)
            //    {
            //        meter = 2000;
            //        jarak = meter/1000;
            //        fJarak[a] = jarak;
            //    }
            //    else if (intInput[a] == 2)
            //    {
            //        meter = 500;
            //        jarak = meter / 1000;
            //        fJarak[a] = jarak;
            //    }
            //    else if (intInput[a] == 3)
            //    {
            //        meter = 1500;
            //        jarak = meter / 1000;
            //        fJarak[a] = jarak;
            //    }
            //    else if (intInput[a] == 4)
            //    {
            //        meter = 300;
            //        jarak = meter / 1000;
            //        fJarak[a] = jarak;
            //    }
            //}
            //foreach(float a in fJarak)
            //{
            //    Console.WriteLine(a+" ");
            //}



        }

        static void soal3()
        {
            DateTime masuk = new DateTime(2019, 8, 20, 07, 50, 00);
            Console.WriteLine(masuk.ToString());
            DateTime keluar = new DateTime(2019, 8, 20, 17, 30, 00);
            Console.WriteLine(keluar.ToString());

            TimeSpan lamaParkir = keluar - masuk;

            int jam = lamaParkir.Hours;
            int menit = lamaParkir.Minutes;
            int totalParkir;

            if (menit > 0)
            {
                jam = jam + 1;
            }
            totalParkir = jam * 3000;
            Console.WriteLine($"Waktu Parkir : {lamaParkir}");
            Console.WriteLine($"Biaya Parkir : {totalParkir}");

        }

        static void soal4()
        {
            string pinjam = "09-06-2019";
            string[] strPinjam = pinjam.Split("-");
            int[] arrPinjam = Array.ConvertAll(strPinjam, int.Parse);
            Console.WriteLine($"Mono meminjam buku tanggal {pinjam}");

            string batas = "12-06-2019";
            string[] strBatas = batas.Split("-");
            int[] arrBatas = Array.ConvertAll(strBatas, int.Parse);
            Console.WriteLine($"Batas pengembalian buku mono {batas}");

            string kembali = "10-07-2019";
            string[] strKembali = kembali.Split("-");
            int[] arrKembali = Array.ConvertAll(strKembali, int.Parse);
            Console.WriteLine($"Mono mengembalikan buku tanggal {kembali}");

            DateTime tglPinjam = new DateTime(arrPinjam[2], arrPinjam[1], arrPinjam[0]);
            DateTime tglKembali = new DateTime(arrKembali[2], arrKembali[1], arrKembali[0]);
            DateTime tglBatas = new DateTime(arrBatas[2], arrBatas[1], arrBatas[0]);

            TimeSpan lamaPinjam = tglKembali - tglPinjam;
            TimeSpan denda = tglKembali - tglBatas;

            Console.WriteLine($"Mono mendapatkan denda {denda} hari");

            int hasil = denda.Days * 500;

            Console.WriteLine($"Denda yang harus dibayar mono : Rp.{hasil}");

            //DateTime pinjam = new DateTime(2019, 09, 06);
            //Console.WriteLine(pinjam.ToString());
            //DateTime balik = new DateTime(2019, 09, 09);
            //Console.WriteLine(balik.ToString());
            //DateTime telat = new DateTime(2019, 10, 07);
            //Console.WriteLine(telat.ToString());

            //TimeSpan lamaPinjam = balik - pinjam;

            //Console.WriteLine(lamaPinjam);

        }

        static void soal5()
        {

            DateTime tanggalMasuk = new DateTime(2021, 02, 26);
            DateTime tanggalOutput = tanggalMasuk;

            int libur = 11;
            int waktuBelajar = 10;
            int temp = 0;
            // int loop = 2;
            bool isTrue = true;
            int i = 0;

            Console.Write($"Tanggal mulai (dd/mm/yyyy) = ");
            Console.WriteLine(tanggalMasuk.ToString("d"));
            Console.WriteLine($"Hari Libur = {libur}");

            while (isTrue)
            {
                i++;
                tanggalOutput = tanggalMasuk.AddDays(i);
                if (tanggalOutput.DayOfWeek == DayOfWeek.Saturday || tanggalOutput.DayOfWeek == DayOfWeek.Sunday || i == libur)
                {
                    continue;
                }
                else
                {
                    temp++;
                }

                if (temp == waktuBelajar)
                {
                    isTrue = false;
                }
            }
            Console.Write("Kelas akan ujian pada = ");
            Console.WriteLine(tanggalOutput.ToString("dd MMMM yyyy"));
            Console.WriteLine("");

            //DateTime tanggalMasuk = new DateTime(2016, 02, 26);
            //Console.WriteLine(tanggalMasuk.ToString());

            //DateTime tanggalSelesai = tanggalMasuk.AddDays(10);
            //int selesai = tanggalSelesai.Day;

            //Console.WriteLine(selesai);
            //Console.WriteLine(tanggalSelesai);

            //bool sabtu = tanggalMasuk.DayOfWeek == DayOfWeek.Saturday ;
            //bool minggu = tanggalMasuk.DayOfWeek == DayOfWeek.Sunday ;

            ////DateTime tanggalUjian = tanggalMasuk.AddDays(selesai);
            ////Console.WriteLine(tanggalUjian);

            //for (int i = 0; i <= selesai; i++)
            //{
            //    if ( i==1 )
            //    {
            //        selesai += 1;
            //        DateTime tanggalUjian = tanggalMasuk.AddDays(selesai);
            //        Console.WriteLine(tanggalUjian);
            //    }
            //    else if ( i == 2) 
            //    {
            //        selesai += 1;
            //        DateTime tanggalUjian = tanggalMasuk.AddDays(selesai);
            //        Console.WriteLine(tanggalUjian);
            //    }
            //    else if ( i == 8)
            //    {
            //        selesai += 1;
            //        DateTime tanggalUjian = tanggalMasuk.AddDays(selesai);
            //        Console.WriteLine(tanggalUjian);
            //    }
            //    else if ( i == 9)
            //    {
            //        selesai += 1;
            //        break;
            //    }

            //}

            //Console.WriteLine(selesai);

            //DateTime tanggalUjian = tanggalMasuk.AddDays(selesai);
            //Console.WriteLine(tanggalUjian);

        }
    }
}

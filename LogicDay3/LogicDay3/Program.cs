﻿using System;

namespace LogicDay3
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("Pilih soal 1-9 :  ");
                string soal = Console.ReadLine();

                switch (soal)
                {
                    case "1":
                        soal1();
                        break;

                    case "2":
                        soal2();
                        break;

                    case "3":
                        soal3();
                        break;

                    case "4":
                        soal4();
                        break;

                    case "5":
                        soal5();
                        break;

                }

                Console.WriteLine("Ulangi proses y/n ? ");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulangi = false;
                }

            }
        }

        static void soal1()
        {
            //int[,] arr = { { 2, 8 }, { 5, 7 }, { 9, 6 } };
            //for (int k= 0; k < 3; k++)
            //{
            //    for (int j = 0; j < 2; j++)
            //    {

            //        Console.Write(arr[]);
            //    }
            //    Console.WriteLine();
            //}


            int i,j;

            Console.Write("Uang Andi : ");
            int andy = int.Parse(Console.ReadLine());

            Console.Write("Harga Baju : ");
            string b = Console.ReadLine();
            string[] baju = b.Split(",");
            int[] hBaju = Array.ConvertAll(baju, int.Parse);

            Console.Write("Harga Celana : ");
            string c = Console.ReadLine();
            string[] celana = c.Split(",");
            int[] hCelana = Array.ConvertAll(celana, int.Parse);

            int[] jum = new int[hCelana.Length];
            int max = 0;
            int total = 0;

            for (i = 0; i < hBaju.Length; i++)
            {
                jum[i] = hBaju[i] + hCelana[i];
            }



            for (j = 0; j < jum.Length; j++)
            {
                if (andy >= jum[j] && jum[j] > max)
                {
                    max = jum[j];

                } 
                

            }

            if ( max == 0)
            {
                Console.WriteLine("Uang anda tidak cukup");

            }
            else
            {
                total = andy - max;

                Console.WriteLine($"Total pembayaran : {max}");
                Console.WriteLine($"Kembali anda : {total}");

            }

            

        }

        static void soal2()
        {
            int i, j, k;

            Console.Write("Uang Andi : ");
            int andy = int.Parse(Console.ReadLine());

            Console.Write("Harga Baju : ");
            string b = Console.ReadLine();
            string[] baju = b.Split(",");
            int[] hBaju = Array.ConvertAll(baju, int.Parse);

            Console.Write("Harga Celana : ");
            string c = Console.ReadLine();
            string[] celana = c.Split(",");
            int[] hCelana = Array.ConvertAll(celana, int.Parse);

            int[] jum = new int[12];

            int max = 0;

            for (i = 0; i < hBaju.Length; i++)
            {
                for (k = 0; k < hCelana.Length; k++)
                {
                    jum[k] = hBaju[i] + hCelana[k];
                    Console.WriteLine($"{hBaju[i]} + {hCelana[k]} = {jum[k]}");
                }

                for (j = 0; j < jum.Length; j++)
                {
                    if (andy >= jum[j] && jum[j] > max)
                    {
                        max = jum[j];

                    }
                    else
                    {
                        continue;
                    }

                }
            }


            Console.WriteLine(max);

            
        }

        static void soal3()
        {
            int[] arr = { 5, 6, 7, 0, 1 };
            int rotasi = 1;
            int a, b;

            for (a=1; a<=rotasi; a++)
            {
                int temp = arr[0];
                for ( b=0; b<arr.Length; b++)
                {
                    if(b < arr.Length - 1)
                    {
                        arr[b] = arr[b + 1];
                    }
                    else
                    {
                        arr[b] = temp;
                    }
                }

                //Console.Write($"{a}. ");

                foreach ( int hasil in arr)
                {
                    Console.Write($"{hasil}, ");
                }
                Console.WriteLine("");

                
            }
            

        }

        static void soal4()
        {
            int menu, alergi;
            string harga;
            string[] hMenu;

            Console.Write("Total Menu : ");
            menu = int.Parse(Console.ReadLine());

            Console.Write("Makanan alergi index ke - ");
            alergi = int.Parse(Console.ReadLine());

            Console.Write("Harga Menu : ");
            harga = Console.ReadLine();
            hMenu = harga.Split(",");
            int[] hargaMenu = Array.ConvertAll(hMenu, int.Parse);

            int totalHarga = 0;
            int totalMakanElsa;
            Console.WriteLine("Uang Elsa : ");
            int uangElsa = int.Parse(Console.ReadLine());
            string sisaElsa;

            for ( int i = 0; i < hMenu.Length ; i++)
            {
                totalHarga += hargaMenu[i];
            }

            totalMakanElsa = totalHarga - hargaMenu[alergi];
            totalMakanElsa /= 2;

            int sisa = uangElsa - totalMakanElsa;

            if ( sisa > 0)
            {
                sisaElsa = sisa.ToString();
            }else if ( sisa == 0)
            {
                sisaElsa = "Uang Pas";
            }
            else
            {
                sisaElsa = "Uang yang dibayarkan" + sisa.ToString();
            }

            Console.WriteLine($"Elsa harus membayar : {totalMakanElsa}");
            Console.WriteLine($"Sisa uang elsa : {sisa}");
            
        }

        static void soal5()
        {
            int panjangStr;

            Console.WriteLine("Input : ");
            string input = Console.ReadLine().ToLower();

            panjangStr = input.Length;

            char[] vocal = new char[panjangStr];
            char[] konsonan = new char[panjangStr];

            for (int i = 0; i<panjangStr; i++)
            {
                if (input[i] == 'a' || input[i] == 'i' || input[i] == 'u' || input[i] == 'e' || input[i] == 'o')
                {
                    vocal[i] = input[i];
                }
                else
                {
                    konsonan[i] = input[i];
                }
            }
            Array.Sort(vocal);
            Array.Sort(konsonan);
            string outVocal = new string(vocal);
            string outKonsonan = new string(konsonan);

            Console.WriteLine($"Vocal : {outVocal}");
            Console.WriteLine($"Konsonan : {outKonsonan}");

        }
    }
}

﻿using System;

namespace PRLogicDay3
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ulangi = true;
            while (ulangi)
            {
                Console.WriteLine("Pilih soal 1-5 :  ");
                string soal = Console.ReadLine();

                switch (soal)
                {
                    case "1":
                        soal1();
                        break;

                    case "2":
                        soal2();
                        break;

                    case "3":
                        soal3();
                        break;

                    case "4":
                        soal4();
                        break;

                    case "5":
                        soal5();
                        break;

                }

                Console.WriteLine("Ulangi proses y/n ? ");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulangi = false;
                }

            }
        }

        static void soal1()
        {
            //string input;
            //Console.Write("Input : ");
            //input = Console.ReadLine();

            Console.Write("Input String: ");

            string sos = Console.ReadLine().ToLower();
            int panjang = sos.Length;
            string[] tampung = new string[panjang];

            int countF = 0;

            for (int i = 0; i < panjang; i = i + 3)
            {
                string pecah = sos.Substring(i, 3);
                tampung[i] = pecah;

                if (tampung[i] == "sos")
                {
                    continue;
                }
                else
                {
                    countF = countF + 1;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Sinyal Yang Benar: SOSSOSSOSSOS");
            Console.WriteLine($"Sinyal yang diterima: {sos.ToUpper()}");
            Console.WriteLine($"Total Sinyal Salah: {countF}");

            Console.ReadKey();
            Console.Clear();
        }

        static void soal2()
        {

            Console.WriteLine("Masukan kalimat : ");
            string inputKalimat = Console.ReadLine();

            string[] arrKalimat = inputKalimat.Split(" ");
            for (int i = 0; i < arrKalimat.Length; i++)
            {
                string kalimat = arrKalimat[i];
                char[] ch = new char[kalimat.Length];
                for (int j = 0; j < kalimat.Length; j++)
                {
                    if (j == 0 || j == kalimat.Length - 1)
                    {
                        ch[j] = kalimat[j];
                    }
                    else
                    {
                        ch[j] = '*';
                    }
                }
                kalimat = new string(ch);
                Console.Write(kalimat + " ");
            }

        }

        static void soal3()
        {
            Console.WriteLine("Masukan kalimat : ");
            string inputKalimat = Console.ReadLine();

            string[] arrKalimat = inputKalimat.Split(" ");
            for (int i = 0; i < arrKalimat.Length; i++)
            {
                string kalimat = arrKalimat[i];
                char[] ch = new char[kalimat.Length];
                for (int j = 0; j < kalimat.Length; j++)
                {
                    if (j == 0 || j == kalimat.Length - 1)
                    {
                        ch[j] = '*';
                    }
                    else
                    {
                        ch[j] = kalimat[j];
                    }
                }
                kalimat = new string(ch);
                Console.Write(kalimat + " ");
            }

        }

        static void soal4()
        {
            Console.Write("Masukkan kata: ");
            string kata = Console.ReadLine();
            char[] katakata = kata.ToCharArray();
            int panjang = kata.Length - 1;
            bool palindrome = true;

            for (int i = 0; i <= panjang; i++)
            {
                katakata[i] = kata[i];
                if (katakata[i] != katakata[panjang])
                {
                    Console.WriteLine("No");
                    palindrome = false;
                    break;
                }
                else
                {
                    panjang--;
                }
            }

            if (palindrome == true)
            {
                Console.WriteLine("Yes");
            }

        }

        static void soal5()
        {
            Console.Write("Masukkan Nilai: ");
            string nilai = Console.ReadLine();
            string[] nilai2 = nilai.Split(",");
            int[] intNilai = Array.ConvertAll(nilai2, int.Parse);
            int[] total = new int[intNilai.Length];
            int[] arrNilai = new int[intNilai.Length];
            int[] pangkasNilai = new int[intNilai.Length];

            for (int i = 0; i < intNilai.Length; i++)
            {
                arrNilai[i] = intNilai[i];
                pangkasNilai[i] = arrNilai[i] % 5;
                if (arrNilai[i] <= 33)
                {
                    arrNilai[i] = arrNilai[i];
                }
                else if (pangkasNilai[i] == 3)
                {
                    arrNilai[i] = arrNilai[i] + 2;
                }
                else if (pangkasNilai[i] == 4)
                {
                    arrNilai[i] = arrNilai[i] + 1;
                }
                //else if (pangkasNilai[i] == 5)
                //{
                //    arrNilai[i] = arrNilai[i];
                //}
                else if (pangkasNilai[i] < 3)
                {
                    arrNilai[i] = arrNilai[i];
                }

                //if (arrNilai[i] < 33)
                //{
                //    arrNilai[i] = arrNilai[i];
                //}
                Console.WriteLine(arrNilai[i]);
            }

            Console.ReadKey();
            Console.Clear();

        }
    }
}
